import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        meta: {
            title: "Formulir Pendataan Bansos",
        },
        component: () => import('../views/FormView.vue')
    },
    {
        path: '/hasil',
        name: 'Hasil',
        meta: {
            title: "Hasil",
        },
        props: true,
        component: () => import('../views/HasilView.vue')
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
