## Project Pendataan Bansos (Frontend JS Project)

Nama    : Marshal Firmansyah
Kelas   : Vue JS

## Desain
Desain yang dibuat sangat sederhana, Warna dominan yang dipakai adalah hijau dan kuning dengan harapan supaya terlihat fresh dan mudah digunakan oleh pengguna.

## Kolom Input

Setiap kolom input memiliki label supaya pengguna lebih mudah melihat mana kolom untuk diisi.

## Validasi Form

Validasi form menggunakan validasi langsung dan tidak langsung :

1. Validasi langsung, yang mana ketika data diinput kurang tepat maka kolom akan merespon serta memberitahukan apa kesalahannya dibawah kolom input yang sedang aktif.
2. Validasi tidak langsung, validasi ini digunakan ketika pengguna menekan tombol kirim. Sistem akan melakukan validasi terhadap semua kolom input apakah telah sesuai atau belum dan Jika salah akan menampilkan informasi kesalahannya dibawah kolom input yang tidak sesuai.

## Notifikasi

Setelah data terkirim maka halaman akan pindah dan manampilkan bahwa data berhasil dikirim.

## Link Deployment 
https://jcc-pendataan-bansos.netlify.app/

## Link Video
https://drive.google.com/file/d/17QPDMM4B3cLzt9kOUJKAAWniYShxEeuM/view?usp=sharing